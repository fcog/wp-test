jQuery(function() {
    // jQuery( ".dragable-image" ).draggable({
    // 	opacity: 0.7,
    // 	zIndex: 20,
	   //  revert: 'invalid',
	   //  helper: 'clone',
	   //  cursorAt: { left: 35 } 	
    // });
    // jQuery( ".topchoice" ).droppable({
    // 	// accept: '.ngg-gallery-thumbnail img',
    // 	// drop: function(event, ui) {
    // 	// 	jQuery(this).append(jQuery(ui.draggable).clone());
    // 	// 	jQuery(".topchoice .ngg-gallery-thumbnail img").addClass("item");
    // 	// 	jQuery(".item").removeClass("ui-draggable ngg-gallery-thumbnail");
    // 	// 	jQuery(".item").draggable({
    //  //            containment: 'parent',
    //  //            grid: [150,150]
    //  //        });
    // 	// }
    // });

	jQuery(".select-image").click(function(){
		var imageId =jQuery(this).attr('id');
		var image = jQuery(this).parent().prev().find('.dragable-image');
		var clone = image.clone();

		if (check_repeated(image.attr('id')) === false)
		{
			if (imageId == "select-top1") {
				jQuery('#top1').empty();
				clone.appendTo('#top1');
				clone.attr('title', 'TOP 1');
			}
			else if (imageId == "select-top2") {
				jQuery('#top2').empty();
				clone.appendTo('#top2');
				clone.attr('title', 'TOP 2');
			}
			else if (imageId == "select-top3") {
				jQuery('#top3').empty();
				clone.appendTo('#top3');
				clone.attr('title', 'TOP 3');
			};

			if (check_all_loaded() === 3) {
				jQuery("#send_rank").empty();
				jQuery("#send_rank").append("<div><input type='button' value='Save Rank' id='save_rank'><input type='button' value='Clear' id='clear_rank'></div>");
			};
		}	
	});

	jQuery('#clear_rank').live("click", function(){
		jQuery('#top1').empty();
		jQuery('#top1').append("TOP 1");		
		jQuery('#top2').empty();
		jQuery('#top2').append("TOP 2");		
		jQuery('#top3').empty();
		jQuery('#top3').append("TOP 3");
		jQuery("#send_rank").empty();
	});

	jQuery('#save_rank').live("click", function(){

		var myObject = new Object();
		myObject.id = [];
		jQuery("#topthree .dragable-image").each(function(){
			myObject.id.push(jQuery(this).attr('id'));
		});

        jQuery.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {images: myObject, userid: jQuery('.user_id').attr('id'), action : 'save_rank', "nonce": nonce},
            success: function (result) {
              alert("Your rank has been saved!");
            }
        });  

    });

});

function check_repeated(newImageId)
{
	var match = false;
	jQuery("#topthree div").each(function(){
		var imageId = jQuery(this).children('img').attr('id');
		if (typeof(imageId) != 'undefined' && imageId == newImageId){
			match = true;
		}
	});

	return match;
}

function check_all_loaded()
{
	var count = 0;
	jQuery("#topthree div").each(function(){
		var imageId = jQuery(this).children('img').attr('id');
		if (typeof(imageId) != 'undefined'){
			count++;
		}
	});

	return count;
}