// //initiate validator on load
// jQuery().ready(function() {
// // validate contact form on keyup and submit
// jQuery("#contest_form").validate({
// //set the rules for the field names
// rules: {
// name: {
// required: true,
// minlength: 2
// },
// email: {
// required: true,
// email: true
// },
// location: {
// required: true,
// minlength: 2
// },
// message: {
// required: true,
// },
// },
// //set messages to appear inline
// messages: {
// name: "Please enter your name",
// email: "Please enter a valid email address",
// location: "Please enter your City/Country",
// message: "Please enter the letters you see in the image"
// }
// });
// });

function validateForm()
{
/* Validating name field */
var x=document.forms["contest_form"]["name"].value;
if (x==null || x=="")
 {
 alert("Name must be filled out");
 return false;
 }
/* Validating email field */
var x=document.forms["contest_form"]["email"].value;
var atpos=x.indexOf("@");
var dotpos=x.lastIndexOf(".");
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
 {
 alert("Not a valid e-mail address");
 return false;
 }
}

function updateCountdown() {
    // 140 is the max message length
    var remaining = 140 - jQuery('.contest-form-message').val().length;
    jQuery('.countdown').text(remaining + ' characters remaining.');
}

jQuery(document).ready(function($) {
    updateCountdown();
    $('.contest-form-message').change(updateCountdown);
    $('.contest-form-message').keyup(updateCountdown);
});