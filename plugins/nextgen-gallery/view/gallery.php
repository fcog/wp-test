<?php 
/**
Template Page for the gallery overview

Follow variables are useable :

	$gallery     : Contain all about the gallery
	$images      : Contain all images, path, title
	$pagination  : Contain the pagination content

 You can check the content when you insert the tag <?php var_dump($variable) ?>
 If you would like to show the timestamp of the image ,you can use <?php echo $exif['created_timestamp'] ?>
**/
global $current_user;
get_currentuserinfo();
?>
<script type="text/javascript">
var ajaxurl = '<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php';
var nonce = "<?php echo js_escape( wp_create_nonce( 'example-ajax-nonce' ) ); ?>";
</script>

<?php if (!defined ('ABSPATH')) die ('No direct access allowed'); ?><?php if (!empty ($gallery)) : ?>

<?php if ( !is_user_logged_in() && $gallery->ID == 31 ): ?>
	<h3>You need to be logged in to vote.</h3>
<?php endif ?> 

<span class="user_id" id="<?php echo $current_user->id ?>"> </span>

<div class="ngg-galleryoverview" id="<?php echo $gallery->anchor ?>">

<?php if ($gallery->show_piclens) { ?>
	<!-- Piclense link -->
	<div class="piclenselink">
		<a class="piclenselink" href="<?php echo $gallery->piclens_link ?>">
			<?php _e('[View with PicLens]','nggallery'); ?>
		</a>
	</div>
<?php } ?>
	
	<!-- Thumbnails -->
	<?php foreach ( $images as $image ) : ?>
	
	<div id="ngg-image-<?php echo $image->pid ?>" class="ngg-gallery-thumbnail-box" <?php echo $image->style ?> >
		<div class="ngg-gallery-thumbnail" >
			<a href="<?php echo $image->imageURL ?>" title="<?php echo $image->description ?>" <?php echo $image->thumbcode ?> >
				<?php if ( !$image->hidden ) { ?>
				<img class="dragable-image" id="image<?php echo $image->pid?>" title="<?php echo $image->alttext ?>" alt="<?php echo $image->alttext ?>" src="<?php echo $image->thumbnailURL ?>" <?php echo $image->size ?> />
				<?php } ?>
			</a>
			<?php echo nggv_imageVoteForm($image->pid); ?> 
		</div>
		<?php if ( is_user_logged_in() && $gallery->ID == 31  ): ?>
		<div id="rank_buttons">
			<div id="select-top1" class="select-image">TOP 1</div>
			<div id="select-top2" class="select-image">TOP 2</div>
			<div id="select-top3" class="select-image">TOP 3</div>
		</div>
		<?php endif ?> 
	</div>
	
	<?php if ( $image->hidden ) continue; ?>
	<?php if ( $gallery->columns > 0 && ++$i % $gallery->columns == 0 ) { ?>
		<br style="clear: both" />
	<?php } ?>

 	<?php endforeach; ?>
 	
	<!-- Pagination -->
 	<?php echo $pagination ?>
 	
</div>
<?php if ( is_user_logged_in() && $gallery->ID == 31 ): ?>
<div id="topthree">
	<h2>Your top 3 choices:</h2>
	<div id="top1" class="topchoice"><span>TOP 1</span></div>
	<div id="top2" class="topchoice"><span>TOP 2</span></div>
	<div id="top3" class="topchoice"><span>TOP 3</span></div>
	<div id="send_rank"></div>
</div>
<?php endif ?> 

<?php endif; ?>